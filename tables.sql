-- Chuong trinh 1 - ImportCSV2DB
CREATE table employee10 (
    id integer NOT NULL UNIQUE,
    "Họ tên" VARCHAR(30) NOT NULL,
    "Ngày tháng năm sinh" DATE,
    "Trình độ học vấn" integer NOT NULL,
    "Cấp bậc công việc" float NOT NULL,
    "Giới tính" BOOLEAN,
    "Tự giới thiệu" TEXT 
);

SELECT * FROM employee10;

-- Tạo table có schema giống hệt như table employee1 bằng cách:
CREATE TABLE employee1 AS (Select * from employee10) WITH NO DATA;
CREATE TABLE employee2 AS (Select * from employee10) WITH NO DATA;
CREATE TABLE employee3 AS (Select * from employee10) WITH NO DATA;

-- Bảng temp là CSDL nguồn lưu dữ liệu chưa qua xử lý
CREATE TABLE temp AS (Select * from employee10) WITH NO DATA;

-- Add thêm column "Ngày học Golang" vào table temp.
ALTER TABLE temp ADD COLUMN "Ngày học Golang" TEXT;

-- Đổi cột "Giới tính" của bảng temp thành kiểu TEXT.
ALTER TABLE temp ALTER COLUMN "Giới tính" TYPE TEXT;

-- Tạo table data_transfrom lưu dữ liệu đã qua xử lý từ table temp.
CREATE TABLE data_transform AS (Select * from temp) WITH NO DATA;

-- Đổi cột "Giới tính" của bảng data_transform:
ALTER TABLE data_transform ALTER COLUMN "Giới tính" TYPE Boolean USING ("Giới tính"::Boolean);

-- \dt để list tất cả các table trong database.
-- \d table_name để xem chi tiết từng table.

-- COPY csvFile to Table in Database
DELETE FROM {csv_table[1]};
COPY {csv_table[1]}(id, ho_ten, ngay_thang_nam_sinh, trinh_do_hoc_van, cap_bac_cong_viec, gioi_tinh, tu_gioi_thieu)
FROM 'D:\\Code\\baitap_python\DataEngineer\\Challenge_3\\csvFile\\{csv_table[0]}'
WITH DELIMITER ','
CSV HEADER;



-- Reference:
-- 1. String Function: https://www.postgresql.org/docs/12/functions-string.html -- replace('abcdefabcdef', 'cd', 'XX')	abXXefabXXef
