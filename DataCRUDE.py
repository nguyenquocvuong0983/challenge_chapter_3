# Viết chương trình dạng dòng lệnh cho phép người dùng chạy lệnh để thực hiện các như cầu sau với CSLD đích trong chương trình DataTransform:
# •	Cho file CSV mới với các dòng dữ liệu có ID đã tồn tại trong CSDL đích. Viết chức năng cho người dùng nạp dữ liệu từ file CSV này để cập nhật lại dữ liệu đang có trong CSDL. Trường hợp một dữ liệu trong file CSV mà chỉ có ID thôi thì có nghĩa là xóa dữ liệu với ID tương ứng trong CSDL đích.
# •	Cho phép người dùng chạy lệnh để export tòan bộ dữ liệu đang có trong CSDL ra file CSV (đường dẫn file được chỉ định trên tham số dòng lệnh).
# •	Xong các chức năng trên hãy tag mã nguồn với tên “v0.0.1”. Sau đó bổ sung thêm chức năng sau (tag với tên “v0.0.2” sau khi hoàn thành):
# o	Cho phép người dùng khai báo câu truy vấn để export dữ liệu ra file CSV. Câu truy vấn có thể truyển trên tham số dòng lệnh, hoặc để sẵn trong file cấu hình.


# RUN FILE bằng lệnh: python DataCRUDE.py 'csv_update_file1.csv' hoặc python DataCRUDE.py 'csv_update_file2.csv'
# Thay đổi 2 file để xem kết quả một cách trực quan.

import psycopg2
import pandas as pd
import yaml
import sys

with open(r'config_infor.yml') as file:    # Mở file config_infor.yml để lấy thông tin kết nối tới database.
    config_infor = yaml.full_load(file) 

if __name__ == "__main__":
    scripts = sys.argv[0]
    csv_file = sys.argv[1]
    conn = psycopg2.connect(
        host = config_infor["host_name"],
        database = config_infor["database"],
        user = config_infor["user_name"],
        password = config_infor["password"],
        port = config_infor["port"],
        connect_timeout = 3
    )
    cur = conn.cursor()


# Các bước thực hiện Query:
# 1. COPY file CSV vào bảng tạm temp2_for_data_crude không chứa dữ liệu đã được tạo sẵn.
# 2. INSERT dữ liệu từ bảng tạm vào bảng đích data_crude:
#       + Khi không có xung đột thì Copy dữ liệu.
#       + Khi có xung đột "id" thì Update dữ liệu.
# 3. Xóa các dòng có giá trị NULL (file CSV chỉ có Id mà không có dữ liệu).

    query = f"""
        DELETE FROM temp2_for_data_crude;

        COPY temp2_for_data_crude FROM 'D:\\Code\\baitap_python\DataEngineer\\Challenge_3\\csvFile\\{csv_file}'
            WITH DELIMITER ','
            CSV HEADER;

        INSERT INTO data_crude
        SELECT * FROM temp2_for_data_crude
        ON CONFLICT(id) DO UPDATE SET "Họ tên" = EXCLUDED."Họ tên",
                                      "Ngày tháng năm sinh" = EXCLUDED."Ngày tháng năm sinh",
                                      "Trình độ học vấn" = EXCLUDED."Trình độ học vấn",
                                      "Cấp bậc công việc" = EXCLUDED."Cấp bậc công việc",
                                      "Giới tính" = EXCLUDED."Giới tính",
                                      "Tự giới thiệu" = EXCLUDED."Tự giới thiệu";
        DELETE FROM data_crude WHERE "Họ tên" IS NULL;
        """
    cur.execute(query=query)        # Thực hiện query.
    conn.commit()                   # Commit để hoàn tất.

    # user_query = "SELECT * FROM data_crude"     # Query của người dùng.

# Xuất query của người dùng ra file CSV bằng cách Copy dữ liệu được người dùng Query ra file CSV.
    query_to_csv = f"""
        COPY ({config_infor["user_query"]})
        TO 'D:\\Code\\baitap_python\\DataEngineer\\Challenge_3\\csvFile\\data_crude.csv'
        DELIMITER ','
        CSV HEADER;
    """

    cur.execute(query=query_to_csv)               # Query tất cả dữ liệu của bảng.



    conn.close()    # Đóng kết nối


# Reference:
# 1. Copy file: https://www.postgresql.org/docs/12/sql-copy.html
# 2. Kết nối database bằng thư viện psycopg2: https://www.psycopg.org/docs/module.html
# 3. Yaml file trong python - pip install pyyaml - https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/