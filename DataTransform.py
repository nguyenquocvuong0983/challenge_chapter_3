# CHƯƠNG TRÌNH DATA_TRANSFORM
# Bảng 'temp' được nhập dữ liệu từ file csv temp.csv có 2 cột dữ liệu "Giới tính" và "Ngày học Golang" sai định dạng do lỗi đánh máy.
# Viết chương trình để xử lý các cột dữ liệu sai này và chèn vào CSDL mới, table 'data_transform'.

# Thông tin kết nối tới Database được lưu trong File 'connect_infor.yml'
# RUN FILE bằng lệnh: python DataTransform.py

import psycopg2
import pandas as pd
import yaml
import sys

# mapping_file = sys.argv[1]     # csvFile/mappingFile.csv - Đường dẫn đến file mappingFile.csv được nhập từ dòng lệnh.
with open(r'connect_infor.yml') as file:    # Mở file connect_infor.yml để lấy thông tin kết nối tới database.
    connect_infor = yaml.full_load(file) 


if __name__ == "__main__":
    scripts = sys.argv[0]   
    conn = psycopg2.connect(
        host = connect_infor["host_name"],
        database = connect_infor["database"],
        user = connect_infor["user_name"],
        password = connect_infor["password"],
        port = connect_infor["port"],
        connect_timeout = 3
    )
    cur = conn.cursor()
# SQL command
# Lấy dữ liệu từ bằng temp, xử lý và insert vào bảng data_transform
# Có 2 cột cần xử lý là "Giới tính" và "Ngày học Golang"
# Dùng hàm ILIKE để kiểm tra giá trị của cột "Giới tính" mà không phân biệt chữ hoa hay chữ thường.
#   + Các giá trị 'nam', 'true', '1' trả về TRUE.
#   + Các giá trị 'nu', 'nữ', '0' trả về FALSE.
# Cột "Ngày học Golang" cần xử lý các trường hợp như:
# 31/12/2021	ngày/tháng/năm: DD/MM/YYYY --> YYYY-MM-DD --> Trường hợp này dùng Regular Expression để kiểm tra.
# 2021-12-31	năm-tháng-ngày: YYYY-MM-DD --> Không cần xử lý
# 20211231	    nămthángngày: YYYYMMDD     --> YYYY-MM-DD --> Trường hợp này dùng điều kiện độ dài text bằng 8.
# Các trường hợp khác nhau thì dùng hàm LEFT, RIGHT, SUSBSTRING với các giá trị khác nhau để tách Y-M-D sau đó ghép
# lại thành chuỗi theo đúng định dạng YYYY-MM-DD.

    query = """
        DELETE FROM data_transform;
        INSERT INTO data_transform (
            select
            id,
            "Họ tên",
            "Ngày tháng năm sinh",
            "Trình độ học vấn",
            "Cấp bậc công việc",
            CASE WHEN "Giới tính" ILIKE ('nam')
                        OR "Giới tính" ILIKE ('true')
                        OR "Giới tính" = '1' THEN TRUE
                WHEN "Giới tính" ILIKE ('nu')
                        OR "Giới tính" ILIKE ('nữ')
                        OR "Giới tính" = '0'
                        OR "Giới tính" ILIKE ('false') THEN FALSE
            END AS "Giới tính",
            "Tự giới thiệu",
            CASE WHEN length("Ngày học Golang") = 8 THEN LEFT("Ngày học Golang", 4) || '-' ||
                        SUBSTRING("Ngày học Golang" from 5 for 2) || '-' || RIGHT("Ngày học Golang", 2)
                WHEN "Ngày học Golang" ~ ('\d{2}/\d{2}/\d{4}') THEN RIGHT("Ngày học Golang", 4) || '-' ||
                        SUBSTRING("Ngày học Golang" from 4 for 2) || '-' || LEFT("Ngày học Golang", 2)
                ELSE "Ngày học Golang"
            END as "Ngày học Golang"
            FROM temp);
    """
    cur.execute(query=query)    # Thực hiện Query để insert dữ liệu.
    conn.commit()               # Commit để xác nhận hoàn tất.

    cur.execute("SELECT * FROM data_transform")
    rows = cur.fetchall()       # Fetch tất cả dữ liệu của bảng data_transform để kiểm tra.
    # for row in rows:          # In từng dòng của bảng theo dạng tuple hoặc tạo DataFrame.
    #     print(row)
    df = pd.DataFrame(rows, index=None)
    df.columns = ['Id', 'Họ tên', 'Ngày tháng năm sinh', 'Trình độ học vấn', 'Cấp bậc', 'Giới tính', 'Tự giới thiệu', 'Ngày học Golang']
    print(df)


    conn.close()    # Đóng kết nối


# Reference:
# 1. Copy file: https://www.postgresql.org/docs/12/sql-copy.html
# 2. Kết nối database bằng thư viện psycopg2: https://www.psycopg.org/docs/module.html
# 3. Yaml file trong python - pip install pyyaml - https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/