# import csv file vào table cho trước trong database.
# - Tag “v0.0.1” có chức năng tối thiểu:
# Import được file 1 CSV đơn giản vào database cho trước.
# Thông tin kết nối database được truyền từ dọng lệnh (gồm host, database type, port, database name, username, password,…)
# - Tag “v0.0.2” có thêm các chức năng tối thiểu:
# Import được 2 file CSV vào 2 table tương ứng trong database cho trước.
# Thông tin kết nối database được bổ sung thêm: hỗ trợ truyền file cấu hình dạng .yml
# - Tag “v0.0.3” có thêm các chức năng tối thiểu:
# Import được nhiều hơn 2 file CSV vào các table tương ứng trong database.
# Thông tin ánh xạ file CSV nào được nạp dữ liệu cho table nào được quy định trong một file cấu hình cũng ở dạng CSV 
# (gồm 2 cột: CSV_File, Table_Name); tên file, hoặc đường dẫn đầy đủ của file csv mapping này được truyền từ tham số.

# 8/11/2021 -- UPDATE file để dùng nhập dữ liệu có cột "Ngày học Golang" dùng cho chương trình DATA_TRANSFORM
# RUN FILE bằng lệnh: python ImportCSV2DB.py 'csvFile/mappingFile_DataTransform.csv'

import psycopg2
import pandas as pd
import yaml
import sys

mapping_file = sys.argv[1]     # csvFile/mappingFile.csv - Đường dẫn đến file mappingFile.csv được nhập từ dòng lệnh.
with open(r'connect_infor.yml') as file:    # Mở file connect_infor.yml để lấy thông tin kết nối tới database.
    connect_infor = yaml.full_load(file) 

# Lưu từng hàng CSV_File, Table_Name dưới dạng tuple.
# Ví dụ: (("employee1", "employee1"), ("employee2", "employee2"),...)

mapping_df = pd.read_csv(mapping_file)
mapping_tuple = mapping_df.apply(tuple, axis=1) 
print(mapping_tuple)

if __name__ == "__main__":
    scripts = sys.argv[0]   
    conn = psycopg2.connect(
        host = connect_infor["host_name"],
        database = connect_infor["database"],
        user = connect_infor["user_name"],
        password = connect_infor["password"],
        port = connect_infor["port"],
        connect_timeout = 3
    )
    cur = conn.cursor()

# Xóa dữ liệu trong bảng trước khi copy csv file vào để không vướng constraint UNIQE(id). Mục đích để tiện cho việc test.
# Quét từng tuple con của mapping_tuple ta được csvFile và table tương ứng.
# CSV_File = csv_table[0]
# Table_Name = csv_table[1]
# Mỗi lần lặp ta copy dữ liệu của CSV File vào Table tương ứng.

    for csv_table in mapping_tuple:
        print("Chèn dữ liệu của {} vào table {}".format(csv_table[0], csv_table[1]))
        query = f"""
            DELETE FROM {csv_table[1]};
            COPY {csv_table[1]}("id", "Họ tên", "Ngày tháng năm sinh", "Trình độ học vấn", "Cấp bậc công việc", "Giới tính", "Tự giới thiệu", "Ngày học Golang")
                FROM 'D:\\Code\\baitap_python\DataEngineer\\Challenge_3\\csvFile\\{csv_table[0]}'
                WITH DELIMITER ','
                CSV HEADER
                ;
            """
        cur.execute(query=query)        # Thực hiện query.
        conn.commit()                   # Commit để hoàn tất.
        cur.execute(f"select * from {csv_table[1]}")  # Query tất cả dữ liệu của bảng.
        rows = cur.fetchall()           # Fetch dữ liệu để kiểm thử.
        df = pd.DataFrame(rows)         # Tạo thành bảng để dễ quan sát.
        
        # Đặt tên các cột tương ứng với bảng.
        df.columns = ['Id', 'Họ tên', 'Ngày sinh', 'Trình độ học vấn', 'Cấp bậc', 'Giới tính', 'Tự giới thiệu', 'Ngày học Golang']
        print(df)
        print('\n')

    conn.close()    # Đóng kết nối


# Reference:
# 1. Copy file: https://www.postgresql.org/docs/12/sql-copy.html
# 2. Kết nối database bằng thư viện psycopg2: https://www.psycopg.org/docs/module.html
# 3. Yaml file trong python - pip install pyyaml - https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/