# Chapter 3

Repo gồm các thành phần:
1. Folder csvFile chứa các file CSV:
    + employee1, employee2, employee3 và mappingFile dùng cho chương trình ImportCSV2DB.py (1) hoặc ImportCSV2DB.exe.
    + temp và mappingFile_DataTransform dùng cho chương trình DataTranform.py (2)
    + csv_update_file1, csv_update_file2 và data_crude dùng cho chương trình DataCRUDE.py (3)
2. Folder dist chứa chương trình ImportCSV2DB.exe dùng cho chương trình 1 hoặc 2.
3. Folder venv là thư mục ảo được tạo bằng python, đã cài đặt các thư viện cần thiết trong môi trường này.
4. Các chương trình 1, 2, 3 từ yêu cầu của Challenge chapter 3.
5. File cấu hình config_infor.yml chứa thông tin kết nối database và SQL command của người dùng.
6. tables.sql chứa các SQL command dùng để tạo bảng, xem dữ liệu bảng, thêm cột, đổi kiểu,...
